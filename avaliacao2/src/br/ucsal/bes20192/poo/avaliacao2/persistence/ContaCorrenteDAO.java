package br.ucsal.bes20192.poo.avaliacao2.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20192.poo.avaliacao2.domain.ContaCorrente;
import br.ucsal.bes20192.poo.avaliacao2.exception.ContaCorrenteNaoEncontradaException;

public class ContaCorrenteDAO {

	private static List<ContaCorrente> contasCorrente = new ArrayList<>();

	public static void salvar(ContaCorrente contaCorrente) {
		contasCorrente.add(contaCorrente);
	}

	public static ContaCorrente pesquisar(Integer codigoAgencia, Integer codigoConta)
			throws ContaCorrenteNaoEncontradaException {
		for (ContaCorrente contaCorrente : contasCorrente) {
			if (contaCorrente.getCodigoAgencia().equals(codigoAgencia)
					&& contaCorrente.getCodigoConta().equals(codigoConta)) {
				return contaCorrente;
			}
		}
		throw new ContaCorrenteNaoEncontradaException(
				"Conta corrente n�o encontrada para a ag�ncia " + codigoAgencia + " conta " + codigoConta);
	}

	public static List<ContaCorrente> obterTodas() {
		return new ArrayList<>(contasCorrente);
	}

}
