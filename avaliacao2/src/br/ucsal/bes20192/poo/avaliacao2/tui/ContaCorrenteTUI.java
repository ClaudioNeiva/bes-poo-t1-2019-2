package br.ucsal.bes20192.poo.avaliacao2.tui;

import java.util.Scanner;

import br.ucsal.bes20192.poo.avaliacao2.business.ContaCorrenteBO;
import br.ucsal.bes20192.poo.avaliacao2.exception.ContaCorrenteNaoEncontradaException;
import br.ucsal.bes20192.poo.avaliacao2.exception.SaldoInsuficienteException;

public class ContaCorrenteTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void sacar() {

		System.out.println("***************** SACAR ********************");

		Integer codigoAgencia;
		Integer codigoConta;
		Double valorSaque;

		System.out.println("Informe o c�digo da ag�ncia:");
		codigoAgencia = sc.nextInt();
		sc.hasNextLine();

		System.out.println("Informe o c�digo da conta:");
		codigoConta = sc.nextInt();
		sc.hasNextLine();

		System.out.println("Informe o valor do saque:");
		valorSaque = sc.nextDouble();
		sc.hasNextLine();

		// Posso utilizar um �nico catch pra capturar as duas exce��es.
		try {
			ContaCorrenteBO.sacar(codigoAgencia, codigoConta, valorSaque);
		} catch (SaldoInsuficienteException | ContaCorrenteNaoEncontradaException e) {
			System.out.println("Erro no saque: " + e.getMessage());
		}

		// Posso utilizar um catch pra capturar cada uma das duas exce��es.
		try {
			ContaCorrenteBO.sacar(codigoAgencia, codigoConta, valorSaque);
		} catch (ContaCorrenteNaoEncontradaException e) {
			System.out.println("Problema nas informa��esd de conta e ag�ncia: " + e.getMessage());
		} catch (SaldoInsuficienteException e) {
			System.out.println("Erro no saque: " + e.getMessage());
			System.out.println(
					"Procure seu gerente para uma linha de cr�dito com juros baixos (apenas 200% aos ano!, aproveite! ;-) )");
		}

	}

}
