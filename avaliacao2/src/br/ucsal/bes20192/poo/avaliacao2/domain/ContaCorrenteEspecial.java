package br.ucsal.bes20192.poo.avaliacao2.domain;

import br.ucsal.bes20192.poo.avaliacao2.exception.SaldoInsuficienteException;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limiteCredito;

	public ContaCorrenteEspecial(Integer codigoAgencia, Integer codigoConta, String nomeTitula, Double saldo,
			Double limiteCredito) {
		super(codigoAgencia, codigoConta, nomeTitula, saldo);
		this.limiteCredito = limiteCredito;
	}

	@Override
	public void sacar(Double valor) throws SaldoInsuficienteException {
		if (valor > saldo + limiteCredito) {
			throw new SaldoInsuficienteException(
					"N�o � poss�vel sacar " + valor + ", pois o saldo dispon�vel � " + saldo);
		}
		saldo -= valor;
	}
	
	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	@Override
	public String toString() {
		return "ContaCorrenteEspecial [limiteCredito=" + limiteCredito + "]";
	}

}
