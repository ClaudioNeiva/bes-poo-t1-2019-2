package br.ucsal.bes20192.poo.avaliacao2.domain;

import br.ucsal.bes20192.poo.avaliacao2.exception.SaldoInsuficienteException;

public class ContaCorrente implements Comparable<ContaCorrente> {

	private Integer codigoAgencia;

	private Integer codigoConta;

	private String nomeTitular;

	protected Double saldo;

	public ContaCorrente(Integer codigoAgencia, Integer codigoConta, String nomeTitula, Double saldo) {
		super();
		this.codigoAgencia = codigoAgencia;
		this.codigoConta = codigoConta;
		this.nomeTitular = nomeTitula;
		this.saldo = saldo;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws SaldoInsuficienteException {
		if (valor > saldo) {
			throw new SaldoInsuficienteException(
					"N�o � poss�vel sacar " + valor + ", pois o saldo dispon�vel � " + saldo);
		}
		saldo -= valor;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public Integer getCodigoAgencia() {
		return codigoAgencia;
	}

	public void setCodigoAgencia(Integer codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}

	public Integer getCodigoConta() {
		return codigoConta;
	}

	public void setCodigoConta(Integer codigoConta) {
		this.codigoConta = codigoConta;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	@Override
	public String toString() {
		return "ContaCorrente [codigoAgencia=" + codigoAgencia + ", codigoConta=" + codigoConta + ", nomeTitula="
				+ nomeTitular + ", saldo=" + saldo + "]";
	}

	@Override
	public int compareTo(ContaCorrente o) {
		return saldo.compareTo(o.saldo);
	}

}
