package br.ucsal.bes20192.poo.avaliacao2.business;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes20192.poo.avaliacao2.domain.ContaCorrente;
import br.ucsal.bes20192.poo.avaliacao2.domain.ContaCorrenteEspecial;
import br.ucsal.bes20192.poo.avaliacao2.exception.ContaCorrenteNaoEncontradaException;
import br.ucsal.bes20192.poo.avaliacao2.exception.SaldoInsuficienteException;
import br.ucsal.bes20192.poo.avaliacao2.persistence.ContaCorrenteDAO;

public class ContaCorrenteBO {

	public static void cadastrarContaCorrenteEspecial(Integer codigoAgencia, Integer codigoConta, String nomeTitula,
			Double saldo, Double limiteCredito) {
		ContaCorrenteEspecial contaCorrenteEspecial = new ContaCorrenteEspecial(codigoAgencia, codigoConta, nomeTitula,
				saldo, limiteCredito);
		ContaCorrenteDAO.salvar(contaCorrenteEspecial);
	}

	public static void sacar(Integer codigoAgencia, Integer codigoConta, Double valor)
			throws SaldoInsuficienteException, ContaCorrenteNaoEncontradaException {
		ContaCorrente contaCorrente = ContaCorrenteDAO.pesquisar(codigoAgencia, codigoConta);
		contaCorrente.sacar(valor);
	}

	public static Set<Integer> obterCodigosAgencia() {
		Set<Integer> codigosAgencia = new HashSet<>();
		for (ContaCorrente contaCorrente : ContaCorrenteDAO.obterTodas()) {
			codigosAgencia.add(contaCorrente.getCodigoAgencia());
		}
		return codigosAgencia;
	}

	// Utilizando o Java 7
	public static List<ContaCorrente> obterContasOrdemSaldo() {
		List<ContaCorrente> contasCorrente = ContaCorrenteDAO.obterTodas();
		Collections.sort(contasCorrente);
		return contasCorrente;
	}

	// Utilizando o Java 8
	public static List<ContaCorrente> obterContasOrdemSaldoJava8() {
		List<ContaCorrente> contasCorrente = ContaCorrenteDAO.obterTodas();

		// Se ContaCorrente implements um Comparable que atende � sua necessidade de
		// ordena��o, voc� pode utilizar um Comparator.naturalOrder.
		// contasCorrente.sort(Comparator.naturalOrder());

		// Ou voc� pode preferir definir o crit�rio de ordena��o aqui.
		contasCorrente.sort(Comparator.comparing(ContaCorrente::consultarSaldo));

		return contasCorrente;
	}

	// Utilizando o Java 7
	public static List<ContaCorrente> obterContasOrdemNomeTitular() {
		List<ContaCorrente> contasCorrente = ContaCorrenteDAO.obterTodas();
		Collections.sort(contasCorrente, new Comparator<ContaCorrente>() {
			@Override
			public int compare(ContaCorrente o1, ContaCorrente o2) {
				return o1.getNomeTitular().compareTo(o2.getNomeTitular());
			}
		});
		return contasCorrente;
	}

	// Utilizando o Java 8
	public static List<ContaCorrente> obterContasOrdemNomeTitularJava8() {
		List<ContaCorrente> contasCorrente = ContaCorrenteDAO.obterTodas();

		// Se ContaCorrente implements um Comparable que atende � sua necessidade de
		// ordena��o, voc� pode utilizar um Comparator.naturalOrder.
		// contasCorrente.sort(Comparator.naturalOrder());

		// Ou voc� pode preferir definir o crit�rio de ordena��o aqui.
		contasCorrente.sort(Comparator.comparing(ContaCorrente::getNomeTitular));

		return contasCorrente;
	}

	
	
}
