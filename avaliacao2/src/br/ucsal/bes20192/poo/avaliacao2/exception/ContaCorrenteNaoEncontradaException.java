package br.ucsal.bes20192.poo.avaliacao2.exception;

public class ContaCorrenteNaoEncontradaException extends Exception {

	private static final long serialVersionUID = 1L;

	public ContaCorrenteNaoEncontradaException(String message) {
		super(message);
	}

}
