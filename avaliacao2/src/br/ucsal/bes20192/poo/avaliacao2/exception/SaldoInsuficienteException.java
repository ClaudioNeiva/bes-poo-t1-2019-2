package br.ucsal.bes20192.poo.avaliacao2.exception;

public class SaldoInsuficienteException extends Exception {

	private static final long serialVersionUID = 1L;

	public SaldoInsuficienteException(String message) {
		super(message);
	}

}
